#!/usr/bin/env bash

set -x
set -e

ctr1=$(buildah from "registry.gitlab.com/pgalonza/basic-images/base-clean:latest")

buildah run "$ctr1" -- dnf install -y elfutils-libelf-devel pkgconfig "@Development Tools"
buildah run "$ctr1" -- dnf install -y jq iptables vim iproute procps
buildah run "$ctr1" -- dnf install -y python3-pip
buildah run "$ctr1" -- pip install --upgrade pip
buildah run "$ctr1" -- /bin/bash -c 'dnf clean packages; \
dnf clean metadata; \
dnf clean all; \
rm -rf \
/var/cache/yum \
/tmp/* \
/var/tmp/*'

## Commit this container to an image name
buildah commit "$ctr1" "${CI_REGISTRY_IMAGE}"
