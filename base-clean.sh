#!/usr/bin/env bash

set -x
set -e

ctr1=$(buildah from "quay.io/almalinux/almalinux:9")

buildah run "$ctr1" -- dnf -y update
buildah run "$ctr1" -- /bin/bash -c 'dnf clean packages; \
dnf clean metadata; \
dnf clean all; \
rm -rf \
/var/cache/yum \
/tmp/* \
/var/tmp/*'

## Commit this container to an image name
buildah commit "$ctr1" "${CI_REGISTRY_IMAGE}"
